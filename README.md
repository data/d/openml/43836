# OpenML dataset: features-and-price-of-computer-components

https://www.openml.org/d/43836

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The most common website that provided computer hardware components we chose the Newegg website it has hardware systems, Buy PC Parts, Laptops, Electronics  More. Now Shipping to Saudi Arabia! Track Order and more with fast shipping.
to determine which the best component with the best price here we can provide you this dataset. 
Content
Implement the Web scraping by using  the python language and using selenium on python, to extract the data from newegg that contain CPU, GPU, power, ram, monitor, storage 
the** data contains**:

the brand name
items_Decribtion
ratings
prices
Category (CPU, GPU,motherboard, ram, powersuplly, storage  )

Acknowledgements
Thank you for the MISK academy and general assembly for guiding us.
Inspiration
we recommend using EDA to clean data and also recommend to build model predictive price or build assumption analysis

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43836) of an [OpenML dataset](https://www.openml.org/d/43836). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43836/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43836/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43836/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

